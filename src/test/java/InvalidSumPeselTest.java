import config.Config;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class InvalidSumPeselTest {

    @BeforeTest
    public void setUp() {
        RestAssured.baseURI = new Config().getApplicationPeselUrl();
    }

    @Test
    public void validateInvalidPesel_InvalidSum(){
        Response response = get("/Pesel?pesel=18112808731");
        String actualBody = response.getBody().asString();
        String expectedBody =("{\"pesel\":\"18112808731\",\"isValid\":false,\"birthDate\":\"1918-11-28T00:00:00\",\"sex\":" +
                "\"Male\",\"errors\":[{\"errorCode\":\"INVC\",\"errorMessage\":\"Check sum is invalid. Check last digit.\"}]}");

        Assert.assertEquals(actualBody,expectedBody);
    }
}
