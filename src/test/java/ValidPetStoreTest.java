import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ValidPetStoreTest {

    @Test
    public void validatePetStore_AddPetToStore(){
        String expectedName = "promil";
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        request.body("{ \"id\": 2, \"category\": { \"id\": 2, \"name\": \"dog\" }, \"name\": \"promil\", \"photoUrls\": " +
                "[ \"string\" ], \"tags\": [ { \"id\": 2, \"name\": \"dog\" } ], \"status\": \"available\" }");

        Response response = request.post("https://petstore.swagger.io/v2/pet");
        int actualid = response.getBody().path("id");
        String actualName = response.getBody().path("name");

        Assert.assertEquals(response.statusCode(),200);
        Assert.assertTrue(actualid>0);
        Assert.assertEquals(actualName,expectedName);
    }
}

