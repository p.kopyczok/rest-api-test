import config.Config;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class ValidatePeselStatusCode {

    @BeforeTest
    public void setUp() {
        RestAssured.baseURI = new Config().getApplicationPeselUrl();
    }

    @Test
    public void validateStatusCode_Ok() {
        Response response = get("/Pesel?pesel=97100599639");
        Assert.assertEquals(response.statusCode(), 200);
    }

    @Test
    public void validateStatusCode_NotOk(){
        Response response =get("/Pesel");
        Assert.assertEquals(response.statusCode(),400);
    }
}
