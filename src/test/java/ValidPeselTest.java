import config.Config;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class ValidPeselTest {

    @BeforeTest
    public void setUp() {
        RestAssured.baseURI = new Config().getApplicationPeselUrl();
    }

    @Test
    public void validateValidPesel_IsValid() {
        Response response = get("/Pesel?pesel=74022828834");

        boolean actualIsValid = response.getBody().path("isValid");
        Assert.assertTrue(actualIsValid);
    }

    @Test
    public void validateValidPesel_BirthDate_20100101() {
        String expectedBirthDate = "2010-01-01T00:00:00";
        Response response = get("/Pesel?pesel=10210194356");
        String actualBirthDate = response.getBody().path("birthDate");

        Assert.assertEquals(actualBirthDate, expectedBirthDate);
    }

    @Test
    public void validateValidPesel_BirthDate_20101231() {
        String expectedBirthDate = "2010-12-31T00:00:00";
        Response response = get("/Pesel?pesel=10323120451");
        String actualBirthDate = response.getBody().path("birthDate");

        Assert.assertEquals(actualBirthDate, expectedBirthDate);
    }

    @Test
    public void validateValidPesel_Sex_Male() {
        String expectedSex = "Male";
        Response response = get("/Pesel?pesel=16321034875");
        String actualSex = response.getBody().path("sex");

        Assert.assertEquals(actualSex, expectedSex);
    }

    @Test
    public void validateValidPesel_Sex_Female() {
        String expectedSex = "Female";
        Response response = get("/Pesel?pesel=11211407700");
        String actualSex = response.getBody().path("sex");

        Assert.assertEquals(actualSex, expectedSex);
    }

    @Test
    public void validateValidPesel_BirthDate_39102208904() {
        String expectedYear = "1939-10-22T00:00:00";
        Response response = get("/Pesel?pesel=39102208904");
        String actualYear = response.getBody().path("birthDate");

        Assert.assertEquals(actualYear, expectedYear);
    }

    @Test
    public void validateValidPesel_BirthDate_88022935906() {
        String expectedBirthDate = "1988-02-29T00:00:00";
        Response response = get("/Pesel?pesel=88022996202");
        String actualBirthDate = response.getBody().path("birthDate");

        Assert.assertEquals(actualBirthDate, expectedBirthDate);
    }

    @Test
    public void validateValidPesel_BirthDate_79043046913() {
        String expectedBirthDate = "1979-04-30T00:00:00";
        Response response = get("/Pesel?pesel=79043046913");
        String actualBirthDate = response.getBody().path("birthDate");

        Assert.assertEquals(actualBirthDate, expectedBirthDate);
    }


}
