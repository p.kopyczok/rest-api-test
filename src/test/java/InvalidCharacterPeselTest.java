import config.Config;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class InvalidCharacterPeselTest {

    @BeforeTest
    public void setUp() {
        RestAssured.baseURI = new Config().getApplicationPeselUrl();
    }

    @Test
    public void validateInvalidPesel_InvalidCharacter_Letter(){
        Response response = get("/Pesel?pesel=740228288E");
        String actualBody = response.getBody().asString();
        String expectedBody = ("{\"pesel\":\"740228288E\",\"isValid\":false,\"birthDate\":null,\"sex\":null,\"errors\":" +
                "[{\"errorCode\":\"NBRQ\",\"errorMessage\":\"Invalid characters. Pesel should be a number.\"}]}");

        Assert.assertEquals(actualBody,expectedBody);
    }

    @Test
    public void validateInvalidPesel_InvalidCharacter_Char(){
        Response response = get("/Pesel?pesel=740/282885");
        String actualBody = response.getBody().asString();
        String expectedBody = ("{\"pesel\":\"740/282885\",\"isValid\":false,\"birthDate\":null,\"sex\":null,\"errors\":" +
                "[{\"errorCode\":\"NBRQ\",\"errorMessage\":\"Invalid characters. Pesel should be a number.\"}]}");

        Assert.assertEquals(actualBody,expectedBody);
    }
}
