import config.Config;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class InvalidDayPeselTest {

    @BeforeTest
    public void setUp() {
        RestAssured.baseURI = new Config().getApplicationPeselUrl();
    }

    @Test
    public void validateInvalidPesel_InvalidDay() {
        Response response = get("/Pesel?pesel=18128087355");
        String actualBody = response.getBody().asString();
        String expectedBody = ("{\"pesel\":\"18128087355\",\"isValid\":false,\"birthDate\":null,\"sex\":\"Male\",\"errors\":" +
                "[{\"errorCode\":\"INVD\",\"errorMessage\":\"Invalid day.\"}]}");

        Assert.assertEquals(actualBody, expectedBody);
    }
}

