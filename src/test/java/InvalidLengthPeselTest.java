import config.Config;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class InvalidLengthPeselTest {

    @BeforeTest
    public void setUp() {
        RestAssured.baseURI = new Config().getApplicationPeselUrl();
    }

    @Test
    public void validateInvalidPesel_Invalidlength_Shorter() {
        Response response = get("/Pesel?pesel=7402282883");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"7402282883\",\"isValid\":false,\"birthDate\":null,\"sex\":null,\"errors\"" +
                ":[{\"errorCode\":\"INVL\",\"errorMessage\":\"Invalid length. Pesel should have exactly 11 digits.\"}]}";

        Assert.assertEquals(actualBody, expectedBody);
    }

    @Test
    public void validateInvalidPesel_Invalidlength_Longer() {
        Response response = get("/Pesel?pesel=740228288322");
        String actualBody = response.getBody().asString();
        String expectedBody = "{\"pesel\":\"740228288322\",\"isValid\":false,\"birthDate\":null,\"sex\":null,\"errors\"" +
                ":[{\"errorCode\":\"INVL\",\"errorMessage\":\"Invalid length. Pesel should have exactly 11 digits.\"}]}";
        Assert.assertEquals(actualBody, expectedBody);
    }
}
